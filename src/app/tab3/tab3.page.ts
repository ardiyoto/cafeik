import { Component } from '@angular/core';
import { Camera,CameraResultType } from '@capacitor/camera';
import { ActionSheetController, NavController } from '@ionic/angular';
Camera

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {

  profile: any = []
  user:any = []
  member:any = []

  constructor(
    public nav:NavController,
    public ac:ActionSheetController,
  ) {

  }


  ionViewDidEnter(){
    this.getProfile()
  }

  getProfile() {
    this.user = JSON.parse(localStorage.getItem("login"))
    this.member = JSON.parse(localStorage.getItem("member"))
  }

  goTo(url:any){
    this.nav.navigateForward(url)
  }

  logout(){
    localStorage.clear()
    this.nav.navigateRoot("/")
  }

  async takePicture() {
    const image = await Camera.getPhoto({
      quality: 100,
      allowEditing: false,
      resultType: CameraResultType.DataUrl,
    });
  
    // Here you get the image as result.
    const resfoto = image.dataUrl;
    this.user.foto = resfoto
    this.member.foto = resfoto

    // Update LocalStorage 
    localStorage.setItem("login", JSON.stringify(this.user))
    localStorage.setItem("member", JSON.stringify(this.member))
  }

}
