import { Location } from '@angular/common';
import { Component } from '@angular/core';
import { App } from '@capacitor/app';
import { StatusBar } from '@capacitor/status-bar';
import { AlertController, Platform } from '@ionic/angular';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  constructor(
    public platform: Platform,
    public loc: Location,
    public alert:AlertController,
  ) {
    this.initializeApp()
  }

  initializeApp() {
    // Change Status Bar
    StatusBar.setBackgroundColor({color:"#f5c174"})

    // Exit Confirm
    this.platform.backButton.subscribeWithPriority(10, () => {
      if (this.loc.isCurrentPathEqualTo('/tabs/tab1') || this.loc.isCurrentPathEqualTo('/')) {

        // Show Exit Alert!
        this.showExitConfirm()      
      } else {

        // Navigate to back page
        this.loc.back();

      }
    });
  }

  showExitConfirm() {
    this.alert.create({
      header: 'Exit App',
      message: 'Do you want to close the app?',
      backdropDismiss: false,
      buttons: [{
        text: 'Stay',
        role: 'cancel',
        handler: () => {
          console.log('Application exit prevented!');
        }
      }, {
        text: 'Exit',
        handler: () => {
          App.exitApp();
        }
      }]
    })
      .then(alert => {
        alert.present();
      });
  }

}
